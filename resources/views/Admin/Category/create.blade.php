@extends('layouts.backend')
@section('content')
<style>
    img {
        border: 1px solid #ddd;
        border-radius: 4px;
        padding: 5px;
        width: 100%;
    }

    img:hover {
        box-shadow: 0 0 2px 1px rgba(0, 140, 186, 0.5);
    }
</style>
<div class="row mb-2">
    <div class="col-sm-6">
        <h1 class="m-0 text-dark">{{ __('messages.list') }} {{ __('messages.category') }}</h1>
    </div>
    <!-- /.col -->
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">{{ __('messages.home') }}</a></li>
            <li class="breadcrumb-item active">{{ __('messages.category') }} </li>
        </ol>
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
</div><!-- /.container-fluid -->
</div>
@if ($errors->any())
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h5><i class="icon fas fa-ban"></i>
        @foreach ($errors->all() as $error)
        {{ $error }}
        @endforeach
    </h5>
</div>
@endif
<form method="post" action="{{ route('category.store') }}" enctype="multipart/form-data">
    <div class="row">
        <!-- left column -->
        <div class="col-md-8">
            <!-- general form elements -->
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">{{ __('messages.add_new') }} {{ __('messages.category') }}</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="name">{{ __('messages.cate_name') }}</label>
                        <abbr title="required">*</abbr>
                        <input type="text" class="form-control" id="name" name="name" onkeyup="ChangeToSlug();" placeholder="{{ __('messages.cate_name') }}">
                    </div>
                    <div class="form-group">
                        <label for="description">{{ __('messages.description') }}</label>
                        <textarea type="text" class="form-control" name="description" id="description" placeholder="{{ __('messages.description') }}"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="type">{{ __('messages.type') }}</label>
                        <abbr title="required">*</abbr><br>
                        @foreach($type as $item)
                        <input type="radio" id="type" name="type" value="{{$item->id}}">
                        <label for="type">{{$item->name}}</label>&#160;&#160;&#160;&#160;
                        @endforeach
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer text-center">
                    <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Save</button>
                    <button type="reset" class="btn btn-success"><i class="fas fa-redo-alt"></i> Reset</button>
                    <button type="button" class="btn btn-danger" onclick="javascript:window.location='{{ route('category.index') }}';"><i class="fas fa-sign-out-alt"></i> Cancel</button>
                </div>
            </div>
            <!-- /.card -->
        </div>
        <div class="col-md-4">
            <!-- general form elements disabled -->
            <div class="card card-light">
                <div class="card-header">
                    <h3 class="card-title">{{ __('messages.static_path') }}</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                        <label>{{ __('messages.slug') }}</label>
                        <input type="text" class="form-control" id="slug" name="slug" placeholder="{{ __('messages.slug') }}" disabled="">
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <div class="card card-light">
                <!-- /.card-header -->
                <div class="card-header">
                    <h3 class="card-title">{{ __('messages.avatar') }}</h3>
                </div>
                <!-- /.card-body -->
                <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
                <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
                <div class="card-body">
                    <div class="row">
                        <label for="img_path">{{ __('messages.avatar') }}</label>
                        <div class="input-group">
                            <input type="text" name="img_path" id="image-input" class="form-control" readonly>
                            <span class="input-group-append">
                                    <button href="{{ asset('/assets/filemanager/dialog.php?relative_url=1&type=1&field_id=image-input') }}"
                                            name="img_path"  type="button" id="avatar" class="btn  btn-primary iframe-btn" >{{ __('messages.upload_avatar') }}</button>
                                </span>
                        </div>
                    </div><br>
                    <div class="row">
                        <img id="avatar_img" class="image-preview" onClick="swipe();"  width="100%" height="50%" border="1px">
                    </div>
                </div>
            </div>
        </div>
        <!--/.col (right) -->
    </div>
</form>
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#avatar_img').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#avatar").change(function() {
        readURL(this);
    });
</script>
<script type="text/javascript">
    $("document").ready(function(){
        setTimeout(function(){
            $("div.alert").remove();
        }, 2000 ); // 2 secs

    });
</script>
{{--config select avatar--}}
<script type="text/javascript">
    $('.iframe-btn').fancybox({
        'type'		: 'iframe',
        'autoScale'    	: false,

    });
    function swipe() {
        var largeImage = document.getElementById('avatar_img');
        var url=largeImage.getAttribute('src');
        window.open(url,'Image','width=largeImage.stylewidth,height=largeImage.style.height,resizable=1');
    }
    function responsive_filemanager_callback(field_id){
        console.log(field_id);
        var url=jQuery('#'+field_id).val();
        $('.image-preview').attr('src','{{ asset('/assets/uploads/') }}' +'/'+ url);
    }
</script>
@endsection
