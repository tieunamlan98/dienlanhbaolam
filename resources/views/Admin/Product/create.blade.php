@extends('layouts.backend')
@section('content')
<style type="text/css">
    img {
        border: 1px solid #ddd;
        border-radius: 4px;
        padding: 5px;
        width: 100%;
    }
    img:hover {
        box-shadow: 0 0 2px 1px rgba(0, 140, 186, 0.5);
    }
    .fancybox-slide--iframe .fancybox-content {
        width  : 800px;
        height : 600px;
        max-width  : 80%;
        max-height : 80%;
        margin: 0;
    }
</style>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/css/fileinput.css" media="all"
      rel="stylesheet" type="text/css">
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" media="all"
      rel="stylesheet" type="text/css">

<div class="row mb-2">
    <div class="col-sm-6">
        <h1 class="m-0 text-dark">{{ __('messages.list') }} {{ __('messages.product') }}</h1>
    </div>
    <!-- /.col -->
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">{{ __('messages.home') }}</a></li>
            <li class="breadcrumb-item active">{{ __('messages.product') }} </li>
        </ol>
    </div>
</div>
@if ($errors->any())
<div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h5><i class="icon fas fa-ban"></i>
        @foreach ($errors->all() as $error)
        {{ $error }}
        @endforeach
    </h5>
</div>
@endif
<form method="post" action="{{ route('product.store') }}" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-md-8">
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">{{ __('messages.add_new') }} {{ __('messages.product') }}</h3>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="name">{{ __('messages.product_name') }}</label>
                        <abbr title="required">*</abbr>
                        <input type="text" class="form-control" id="name" name="name" onkeyup="ChangeToSlug();" placeholder="{{ __('messages.cate_name') }}" required>
                    </div>
                    <div class="form-group">
                        <label for="description">{{ __('messages.description') }}</label>
                        <abbr title="required">*</abbr>
                        <textarea type="text" class="form-control" name="description" id="description" placeholder="{{ __('messages.description') }}"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="gallerys">{{ __('messages.gallerys') }}</label>
                        <input type="file" id="file-1" name="gallery[]" multiple class="file" data-overwrite-initial="false">
                    </div>
                </div>
                <div class="card-footer text-center">
                    <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> {{ __('messages.save') }}</button>
                    <button type="reset" class="btn btn-success"><i class="fas fa-redo-alt"></i> {{ __('messages.reset') }}</button>
                    <button type="button" class="btn btn-danger" onclick="javascript:window.location='{{ route('product.index') }}';"><i class="fas fa-sign-out-alt"></i> {{ __('messages.cancel') }}</button>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card card-light">
                <div class="card-header">
                    <h3 class="card-title">{{ __('messages.static_path') }}</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <label>{{ __('messages.slug') }}</label>
                        <input type="text" class="form-control" id="slug" name="slug" placeholder="{{ __('messages.slug') }}" disabled="">
                    </div>
                </div>
            </div>
            <div class="card card-light">
                <div class="card-header">
                    <h3 class="card-title">{{ __('messages.detail') }}</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <label for="quantity">{{ __('messages.category') }}</label>
                            <abbr title="required">*</abbr>
                            <select class="form-control" id="category_id" name="category_id">
                                @foreach($category as $item)
                                    <option value="{{$item->id }}">{{$item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="quantity">{{ __('messages.quantity') }}</label>
                            <abbr title="required">*</abbr>
                            <input type="text" class="form-control" name="quantity" id="quantity" placeholder="{{ __('messages.quantity') }}" required>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-9">
                            <label for="price">{{ __('messages.price') }}</label>
                            <abbr title="required">*</abbr>
                            <input type="text" class="form-control"  id="price" name="price" placeholder="{{ __('messages.price') }}">
                        </div>
                        <div class="col-md-3">
                            <label for="price">{{ __('messages.unit') }}</label>
                            <input type="text" class="form-control" placeholder="{{ __('VNĐ') }}" readonly >
                        </div>
                    </div>
                </div>
            </div>
            <div class="card card-light">
                <div class="card-header">
                    <h3 class="card-title">{{ __('messages.avatar') }}</h3>
                </div>
                <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
                <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
                <div class="card-body">
                    <div class="row">
                        <label for="avatar">{{ __('messages.avatar') }}</label>
                        <div class="input-group">
                            <input type="text" name="avatar" id="image-input" class="form-control" readonly>
                            <span class="input-group-append">
                                    <button href="{{ asset('/assets/filemanager/dialog.php?relative_url=1&type=1&field_id=image-input') }}"
                                            name="img_path"  type="button" id="avatar" class="btn  btn-primary iframe-btn" >{{ __('messages.upload_avatar') }}</button>
                                </span>
                        </div>
                    </div><br>
                    <div class="row">
                        <img id="avatar_img" class="image-preview" onClick="swipe();"  width="100%" height="50%" border="1px">
                    </div>
                </div>
            </div>
            <div class="card card-light">

            </div>
        </div>
    </div>
</form>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/js/fileinput.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/themes/fa/theme.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.5.3/umd/popper.min.js" type="text/javascript"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" type="text/javascript"></script>
{{--select multi image input type file--}}
<script type="text/javascript">
    $("#file-1").fileinput({
        theme: 'fa',
        showUpload:false,
        showCancel:false,
        allowedFileExtensions:['jpeg','png','jpg','gif','svg'],
        overwriteInitial:false,
        maxFileSize:2048,
        maxFileNum:5,
        slugCallback:function (filename){
            return filename.replace('(','_').replace(']','_');
        }
    });
</script>
{{--config input type text allow only numberic--}}
<script type="text/javascript">
    function setInputFilter(textbox, inputFilter) {
        ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
            textbox.addEventListener(event, function() {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
            });
        });
    }
    setInputFilter(document.getElementById("quantity"), function(value) { return /^-?\d*$/.test(value); });
    setInputFilter(document.getElementById("price"), function(value) { return /^-?\d*$/.test(value); });

</script>
<script type="text/javascript">
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#avatar_img').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#avatar").change(function() {
        readURL(this);
    });
</script>
{{--config select 2--}}
<script type="text/javascript">
    $("#category_id").select2({placeholder: '{{ __('messages.select3') }}', minimumInputLength: 2, theme: 'bootstrap4',});
</script>
{{--config select avatar--}}
<script type="text/javascript">
    $('.iframe-btn').fancybox({
        'type'		: 'iframe',
        'autoScale'    	: false,

    });
    function swipe() {
        var largeImage = document.getElementById('avatar_img');
        var url=largeImage.getAttribute('src');
        window.open(url,'Image','width=largeImage.stylewidth,height=largeImage.style.height,resizable=1');
    }
    function responsive_filemanager_callback(field_id){
        console.log(field_id);
        var url=jQuery('#'+field_id).val();
        $('.image-preview').attr('src','{{ asset('/assets/uploads/') }}' +'/'+ url);
    }
</script>
@endsection
